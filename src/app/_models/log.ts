﻿export class Log {
    id: number;
    message: string;
    log_level: string;
    app_id: number;
    created_at: Date;
}
