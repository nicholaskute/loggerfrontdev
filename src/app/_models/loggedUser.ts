﻿export class LoggedUser {
  authorities: any;
  details: {
    remoteAddress: string,
    sessionId: string
  };
  authenticated: boolean;
  principal: {
    id: number;
    name: string;
    username: string;
    email: string;
    authorities: any;
    enabled: true,
    accountNonExpired: true,
    accountNonLocked: true,
    credentialsNonExpired: true
  };
  credentials: any;
  name: string;

}
