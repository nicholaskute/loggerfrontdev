﻿export class App {
    id: number;
    name: string;
    code: string;
    log_pattern: string;
    created_at: Date;
}
