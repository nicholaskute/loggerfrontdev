﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';
import * as myGlobals from '../globals';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${myGlobals.apiUrl}/api/users`);
    }

    getById(id: number) {
        return this.http.get(`${myGlobals.apiUrl}/users/` + id);
    }

    register(user: User) {
        return this.http.post(`${myGlobals.apiUrl}/api/auth/register`, user);
    }

    update(user: User) {
        return this.http.put(`${myGlobals.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${myGlobals.apiUrl}/users/` + id);
    }
}
