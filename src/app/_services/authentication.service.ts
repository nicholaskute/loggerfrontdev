﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as myGlobals from '../globals';
import { Account } from '../_interfaces/account.interface';
import { AuthToken } from '../_interfaces/authToken.interface';


@Injectable()
export class AuthenticationService {

    private tokenData = { 'username': '', 'password': '' };

    authToken = {} as AuthToken;
    constructor(private http: HttpClient) { }

    login(account: Account) {

      // get user data
      this.tokenData.username = account.username;
      this.tokenData.password = account.password

      return this.http.post<any>(`${myGlobals.apiUrl}/api/auth/login`,
        JSON.stringify(this.tokenData), this.getArgHeaders());

    }

    getCurrentUserData() {

      // get logged in user data
      return this.http.get<any>(`${myGlobals.apiUrl}/api/users/currentuser`, this.getArgHeaders());

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    private getArgHeaders(): any {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      return httpOptions;
    }


}
