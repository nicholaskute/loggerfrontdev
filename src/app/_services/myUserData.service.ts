import { Injectable } from '@angular/core';

import { AuthToken } from '../_interfaces/authToken.interface';
import {User} from "../_models/user";
import {LoggedUser} from "../_models/loggedUser";

@Injectable()
export class MyUserDataService {

  HAS_LOGGED_IN = 'hasLoggedIn';
  CURRENT_USER = 'currentUser';
  AUTH_TOKEN = 'authToken';

  constructor(
  ) {}

  setUser(user) {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }

  public login(): void {
    localStorage.setItem(this.HAS_LOGGED_IN, "1");
  }

  logout(): void {
    localStorage.remove(this.HAS_LOGGED_IN);
    localStorage.remove(this.CURRENT_USER);
  }

  getUser() {
    return localStorage.getItem(this.CURRENT_USER);
  }

  setTokenData(token) {
    localStorage.setItem(this.AUTH_TOKEN, JSON.stringify(token));
  }

  getTokenData(): Promise<AuthToken> {
    return localStorage.get(this.AUTH_TOKEN).then((value) => {
      return value;
    });
  }

  hasLoggedIn(): Promise<boolean> {
    return localStorage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  }
}
