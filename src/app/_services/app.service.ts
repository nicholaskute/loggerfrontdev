﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { App } from '../_models';
import * as myGlobals from '../globals';

@Injectable()
export class AppService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<App[]>(`${myGlobals.apiUrl}/api/applications`);
    }

    getById(id: number) {
        return this.http.get(`${myGlobals.apiUrl}/applications/` + id);
    }

    create(app: App) {
        return this.http.post(`${myGlobals.apiUrl}/api/applications`, app);
    }

    update(app: App) {
        return this.http.put(`${myGlobals.apiUrl}/applications/` + app.id, app);
    }

    delete(id: number) {
        return this.http.delete(`${myGlobals.apiUrl}/applications/` + id);
    }
}
