﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { App } from '../_models';
import * as myGlobals from '../globals';
import {Log} from "../_models/log";

@Injectable()
export class LogsService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Log[]>(`${myGlobals.apiUrl}/api/application-logs`);
    }

    getById(id: number) {
        return this.http.get(`${myGlobals.apiUrl}/application-logs/` + id);
    }

    create(log: Log) {
        return this.http.post(`${myGlobals.apiUrl}/api/application-logs`, log);
    }

    update(log: Log) {
        return this.http.put(`${myGlobals.apiUrl}/application-logs/` + log.id, log);
    }

    delete(id: number) {
        return this.http.delete(`${myGlobals.apiUrl}/application-logs/` + id);
    }
}
