﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home';
import { LoginComponent } from './components/auth/login';
import { RegisterComponent } from './components/auth/register';
import { AuthGuard } from './_guards';

//import {UsersComponent} from './components/user/list/users.component';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' /* , canActivate: [AuthGuard] */ },

    //{ path: 'users', component: UsersComponent /* , canActivate: [AuthGuard]  */ },

    // otherwise redirect to page notfound
    // { path: '**', redirectTo: '' }
    { path: '**', component: PageNotFoundComponent }
];

export const routing = RouterModule.forRoot(appRoutes);
