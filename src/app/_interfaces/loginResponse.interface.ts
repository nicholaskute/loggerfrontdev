﻿import DateTimeFormat = Intl.DateTimeFormat;

export interface LoginResponse {

  result?: {
    tokenType?:string;
    accessToken?:string;
  }
  error?:
  {
    "timestamp": DateTimeFormat,
    "status": number,
    "error": string,
    "message": string,
    "path": string
  }

}
