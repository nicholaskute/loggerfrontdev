import { NgModule } from '@angular/core';

import {AppsRoutingModule} from './apps-routing.module';
import {SharedModule} from '../shared/shared.module';

import {CreateAppComponent} from './create-app/create-app.component';
import {ListAppsComponent} from './list-apps/list-apps.component';
import {ViewAppsComponent} from './view-app/view-apps.component';
import {EditAppComponent} from './edit-app/edit-app.component';

@NgModule({
  imports: [
    AppsRoutingModule,
    SharedModule
  ],
  declarations: [
    CreateAppComponent,
    ListAppsComponent,
    ViewAppsComponent,
    EditAppComponent
  ],
  exports: [
    CreateAppComponent,
    ListAppsComponent,
    ViewAppsComponent,
    EditAppComponent
  ]
})

export class AppsModule { }
