import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { App } from '../../../_models/index';
import { AppService, MyUserDataService } from '../../../_services/index';

@Component({
  selector: 'app-view-apps',
  templateUrl: './view-apps.component.html',
  styleUrls: ['./view-apps.component.css']
})
export class ViewAppsComponent implements OnInit {
  currentUser: any;
  apps: App[] = [];

  constructor(private appService: AppService, private myUserDataService: MyUserDataService) {
    // get logged in user data
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser = this.myUserDataService.getUser();
  }

  ngOnInit() {
    this.loadAllApps();
  }

  deleteUser(id: number) {
    this.appService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllApps()
    });
  }

  private loadAllApps() {
    this.appService.getAll().pipe(first()).subscribe(apps => {
      this.apps = apps;
    });
  }
}


