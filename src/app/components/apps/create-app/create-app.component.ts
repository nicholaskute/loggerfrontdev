import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import {AlertService, AppService, UserService} from '../../../_services';

@Component({
  selector: 'app-create-app',
  templateUrl: './create-app.component.html',
  styleUrls: ['./create-app.component.css']
})
export class CreateAppComponent implements OnInit {
  createAppForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private appService: AppService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.createAppForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      log_pattern: ['', Validators.required]});
  }

  // convenience getter for easy access to form fields
  get f() { return this.createAppForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createAppForm.invalid) {
      return;
    }

    this.loading = true;
    this.appService.create(this.createAppForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('App created successfully', true);
          this.router.navigate(['/apps']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}


