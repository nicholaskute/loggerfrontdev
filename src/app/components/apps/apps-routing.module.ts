import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../../_guards';

import {CreateAppComponent} from './create-app/create-app.component';
import {ViewAppsComponent} from './view-app/view-apps.component';
import {ListAppsComponent} from './list-apps/list-apps.component';
import {EditAppComponent} from './edit-app/edit-app.component';

const appRoutes: Routes = [
    { path: 'apps/create', component: CreateAppComponent, canActivate: [AuthGuard]  },
    { path: 'apps', component: ListAppsComponent, canActivate: [AuthGuard] },
    { path: 'apps/:id', component: ViewAppsComponent, canActivate: [AuthGuard]  },
    { path: 'apps/edit/:id', component: EditAppComponent, canActivate: [AuthGuard]  },
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [ RouterModule ]

})

export class AppsRoutingModule { }
