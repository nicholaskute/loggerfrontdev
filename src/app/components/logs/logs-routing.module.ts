import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ViewLogsComponent} from './view-logs/view-logs.component';

const appRoutes: Routes = [
    { path: 'logs', component: ViewLogsComponent  }
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [ RouterModule ]

})

export class LogsRoutingModule { }
