import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ViewLogsComponent } from './view-logs/view-logs.component';
import { LogsRoutingModule } from './logs-routing.module';

@NgModule({
  imports: [
    LogsRoutingModule,
    SharedModule
  ],
  declarations: [
    ViewLogsComponent
  ],
  exports: [
    ViewLogsComponent
  ]
})
export class LogsModule { }
