import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { Log } from '../../../_models/index';
import { LogsService, MyUserDataService } from '../../../_services/index';

@Component({
  selector: 'app-view-logs',
  templateUrl: './view-logs.component.html',
  styleUrls: ['./view-logs.component.css']
})
export class ViewLogsComponent implements OnInit {
  currentUser: any;
  logs: Log[] = [];

  constructor(private logsservice: LogsService, private myUserDataService: MyUserDataService) {
    this.currentUser = this.myUserDataService.getUser();
  }

  ngOnInit() {
    this.loadAllLogs();
  }

  deleteLog(id: number) {
    this.logsservice.delete(id).pipe(first()).subscribe(() => {
      this.loadAllLogs()
    });
  }

  private loadAllLogs() {
    this.logsservice.getAll().pipe(first()).subscribe(logs => {
      this.logs = logs;
    });
  }
}



