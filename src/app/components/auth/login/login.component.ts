﻿﻿import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService, AuthenticationService } from '../../../_services/index';
import { MyUserDataService } from '../../../_services/index';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {

    @Output() getLoggedInUser: EventEmitter<any> = new EventEmitter();

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    private accountData = { 'username': '', 'password': '' };

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private myUserDataService: MyUserDataService) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        this.accountData.username = this.f.username.value;
        this.accountData.password = this.f.password.value;

        this.authenticationService.login(this.accountData)
            .subscribe(
                data => {
                    this.loading = false;
                    this.myUserDataService.setTokenData(data);
                    // set current user data
                    this.setCurrentUserData();
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }


    setCurrentUserData() {
      this.authenticationService.getCurrentUserData()
        .subscribe(
          (data) => {
            // console.log(data);
            this.myUserDataService.setUser(data);
            //emit the user data
            this.getLoggedInUser.emit(data);
            this.router.navigate([this.returnUrl]);
          },
          error => {
            this.alertService.error(error);
          });
    }

    /*isFieldInvalid(field: string) {
      return (
        (!this.loginForm.get(field).valid && this.loginForm.get(field).touched) ||
        (this.loginForm.get(field).untouched && this.submitted)
      );
    }*/

}
