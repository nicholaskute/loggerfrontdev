import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { LoggedUser } from '../../../_models';
import { MyUserDataService } from '../../../_services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentUser: string;

  constructor(private myUserDataService: MyUserDataService) {
    this.currentUser = myUserDataService.getUser();
  }

  ngOnInit() {
  }

}

