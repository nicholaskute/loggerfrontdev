import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../../_guards';
import { UsersComponent } from './list/users.component';
import { ViewUserComponent } from './view/view-user/view-user.component';
import { EditUserComponent } from './edit/edit-user.component';
import { CreateUserComponent } from './add/create-user.component';

const appRoutes: Routes = [
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard]  },
    { path: 'users/create', component: CreateUserComponent, canActivate: [AuthGuard]  },
    { path: 'users/:id', component: ViewUserComponent, canActivate: [AuthGuard] },
    { path: 'users/edit/:id', component: EditUserComponent, canActivate: [AuthGuard]  },
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [ RouterModule ]

})

export class UserRoutingModule { }
