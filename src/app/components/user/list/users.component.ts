import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User, AuthToken } from '../../../_models/index';
import { UserService, MyUserDataService } from '../../../_services/index';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  currentUser: any;
  users: User[] = [];

  constructor(private userService: UserService, private myUserDataService: MyUserDataService) {
    // get logged in user data
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser = this.myUserDataService.getUser();
  }

  ngOnInit() {
    this.loadAllUsers();
  }

  deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllUsers()
    });
  }

  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }
}

