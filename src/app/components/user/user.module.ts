import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user-routing.module';

import { UsersComponent } from './list/users.component';
import { ViewUserComponent } from './view/view-user/view-user.component';
import { EditUserComponent } from './edit/edit-user.component';

import { SharedModule } from '../shared/shared.module';
import { CreateUserComponent } from './add/create-user.component';

@NgModule({
  imports: [
    UserRoutingModule,
    SharedModule
  ],
  declarations: [
    UsersComponent,
    ViewUserComponent,
    EditUserComponent,
    CreateUserComponent
  ]
})

export class UserModule { }
