﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import {AlertService, AuthenticationService, UserService, MyUserDataService, AppService, LogsService} from './_services';
import { HomeComponent } from './components/home';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';

import { AuthModule } from './components/auth/auth.module';
import { UserModule } from './components/user/user.module';
import { AppsModule } from './components/apps/apps.module';
import { LogsModule } from './components/logs/logs.module';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        LogsModule,
        AuthModule,
        UserModule,
        AppsModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        HeaderComponent,
        FooterComponent,
        PageNotFoundComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        AppService,
        LogsService,
        MyUserDataService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
